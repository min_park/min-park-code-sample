import db from '../models';
import constants from '../helpers/constants';
import cacheHelper from '../helpers/cacheHelper';
import mathHelper from '../helpers/mathHelper';

const { DeliveryPackage } = db.models;

function getAllPackageList() {
    const cacheKey = constants.CacheKeys.DeliveryPackageAll;
    return cacheHelper.get(cacheKey)
        .then(cacheResult => {
            if (cacheResult) {
                return Promise.resolve(cacheResult);
            }
            return DeliveryPackage.findAll({order: [['totalVolume', 'ASC']]})
                .then(packageList => {
                    cacheHelper.set(cacheKey, packageList);
                    return packageList;
                });
        });
}

function getPackageSpec(totalVolume, totalWeight) {
    return getAllPackageList()
        .then(packageList => {
            const bestPackage = packageList.filter(p => p.netVolume >= totalVolume)[0];
            return {
                width: bestPackage.width,
                length: bestPackage.length,
                height: bestPackage.height,
                weight: mathHelper.roundNumber(bestPackage.weight + totalWeight, 1)
            };
        });
}

export default {
    getPackageSpec
};
