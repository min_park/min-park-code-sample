import config from 'config';
import constants from '../../helpers/constants';
import jwtHelper from '../../helpers/jwtHelper';

export default (req, res, next) => {
    const token = req.body.jwt || req.query.jwt || req.headers.jwt;
    return jwtHelper.verifyJwt(token, config.jwtSecretKey, true, constants.ERROR_CODES.loginUserTokenInvalid)
        .then(decodedResult => {
            if (decodedResult) {
                req.customerId = decodedResult.customerId;
            }
            return next();
        })
        .catch(next);
};
