import bodyParser from 'body-parser';
import express from 'express';
import home from './middleware/home';
import docs from './middleware/docs';
import notFound from './middleware/notFound';
import error from './middleware/error';
import logHelper from '../helpers/logHelper';

export default class {
    constructor(params) {
        this.params = params;
        this.middleware = (req, res, next) => { next(); };
        this.init();
    }

    init() {
        this.validateParams();
        this.initExpress();
        this.initRouteMiddleware();
    }

    validateParams() {
        if (!this.params.name) throw new Error('missing name');
        if (!this.params.routes) throw new Error('missing routes');
        if (!this.params.docs) throw new Error('missing documentation');
        if (this.params.middleware) this.middleware = this.params.middleware;
    }

    initExpress() {
        let port = process.env.PORT || 4000;
        this.server = express();
        this.server.set('port', port);
        this.server.set('name', this.params.name);
        this.server.set('docs', this.params.docs);
        this.server.use(bodyParser.json());
        this.server.use(this.middleware);
    }

    initRouteMiddleware() {
        this.server.get('/', home);
        this.server.get('/docs', docs);
        this.server.use(this.params.routes);
        this.server.use(notFound);
        this.server.use(error);
    }

    start() {
        this.http = this.server.listen(this.server.get('port'), () => {
            logHelper.info(`started at port: ${this.server.get('port')}`);
        });
    }

    stop() {
        if (this.http) this.http.close();
    }
}
