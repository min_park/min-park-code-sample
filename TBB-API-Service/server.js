import Server from './core/baseServer';
import routes from './routes';

export default function() {
    const server = new Server({
        name: 'TBB_API_SERVICE',
        docs: './docs/api.raml',
        routes: routes
    });

    server.start();
    return server;
}
