export default function(sequelize, dataTypes) {
    /*eslint-disable */
    return sequelize.define('Customer',
        {
            id: {
                type: dataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            email: {
                type: dataTypes.STRING(80),
                unique: true,
                allowNull: false,
                validate: {
                    isEmail: {msg: "Email is invalid format."}}
            },
            password: {
                type: dataTypes.STRING(500),
                allowNull: false
            },
            firstName: {
                type: dataTypes.STRING(50),
                allowNull: false,
            },
            lastName: {
                type: dataTypes.STRING(50),
            },
            contactNumber: {
                type: dataTypes.STRING(50),
            }
        },
        {
            indexes: [
                {
                    unique: true,
                    fields: ['email']
                }
            ],
            freezeTableName: true
        }
    );
    /*eslint-enable */
}
