# README #

Code samples to represent Min's experience in an e-commerce web system (thebrownbottle.com.au) using React.js, Redux, Express.js, Node.js, PostgreSQL, etc.

### Note ###

* Demonstration purpose only
* Will not work due to it's code snippets from working version repositories

### Main Tech Stack ###

* React.js
* Redux
* Express.js
* Node.js
* PostgreSQL
