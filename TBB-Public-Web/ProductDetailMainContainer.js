import { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';
import { bindActionCreators } from 'redux';
import * as actions from '../../redux/actions/productDetailActions';
import * as cartActions from '../../redux/actions/cartActions';
import asset from '../../redux/constants/assetEndpoints';
import messageHelper from '../../helpers/messageHelper';
import ProductDetailMain from '../../components/productDetail/ProductDetailMain';

export class ProductDetailMainContainer extends Component {
    constructor(props) {
        super(props);

        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.handleResetCartErrorMsg = this.handleResetCartErrorMsg.bind(this);
    }

    componentDidMount() {
        this.handleResetCartErrorMsg();
        actions.onProductSelected(this.props.params.product_code);
    }

    handleAddToCart(productVarietyId, quantity) {
        const {cartActions, authState, globalState} = this.props;
        const {product} = this.props.appState;

        if (product.isSample && !authState.isLoginUser) {
            browserHistory.push(`/login?redirect_to=/product/${product.code}`);
        } else {
            cartActions.onAddUpToCart(
                authState.jwt,
                globalState.clientCookieKey,
                productVarietyId,
                quantity,
                messageHelper.showBottomSidebar);
        }
    }

    handleResetCartErrorMsg() {
        this.props.cartActions.onResetErrorMsg();
    }

    render() {
        const { appState, cartState } = this.props;
        const { product } = appState;

        return (
            <div>
                <Helmet
                    title={product.name}
                    meta={seoHelper.getProductMetaTags(
                        product.name,
                        product.description,
                        this.props.location.pathname,
                        asset.PRODUCT_LIST_IMAGE_URL + product.code + '/' + product.images[0],
                        product.productVarieties[0].price
                    )}
                />
                <ProductDetailMain
                    product={product}
                    isLoginUser={isLoginUser}
                    cartErrorMsg={cartState.errorMsg}
                    isFetching={cartState.isFetching}
                    onAddToCart={this.handleAddToCart}
                    onResetCartErrorMsg={this.handleResetCartErrorMsg}
                    onBackToList={this.handleBackToList}
                />
            </div>
        );
    }
}

ProductDetailMainContainer.need = [
    (params) => actions.onProductSelected(params.product_code)
];

ProductDetailMainContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    cartActions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    cartState: PropTypes.object.isRequired,
    globalState: PropTypes.shape({
        clientCookieKey: PropTypes.string.isRequired
    }).isRequired,
    authState: PropTypes.object.isRequired,
    params: PropTypes.shape({
        product_code: PropTypes.string.isRequired
    }).isRequired,
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired
    }).isRequired
};

function mapStateToProps(state) {
    return {
        appState: state.productDetailAppState,
        cartState: state.cartAppState,
        globalState: state.globalAppState,
        authState: state.authAppState
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch),
        cartActions: bindActionCreators(cartActions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductDetailMainContainer);
