var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var AssetsPlugin = require('assets-webpack-plugin');
var assetsPluginInstance = new AssetsPlugin();

module.exports = {
    devtool: 'source-map',

    entry: __dirname + "/client/index.js",

    output: {
        path: __dirname + '/static/dist/',
        publicPath: '/dist/',
        filename: "[name]-bundle-[hash].js"
    },

    resolve: {
        extensions: ['', '.js', '.jsx'],
    },

    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style', 'css?modules'),
            },
            {
                test: /\.jsx*$/,
                exclude: /node_modules/,
                loader: 'babel',
            }
        ],
    },

    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production'),
                'API_ENV': JSON.stringify(process.env.API_ENV)
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compressor: {
                warnings: false,
            }
        }),
        new ExtractTextPlugin("app.css"),
        assetsPluginInstance
    ],
};
