import { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Constants = from './constants';
import ProductDetailItem from './ProductDetailItem';

export default class ProductDetailMain extends Component {
    render() {
        const { product, isLoginUser, cartErrorMsg, isFetching, onAddToCart, onResetCartErrorMsg, onBackToList } = this.props;

        return (
            <ReactCSSTransitionGroup
                transitionName = Constants.Transaction.Change
                component="div"
                transitionAppear = {true}
                transitionAppearTimeout={1000}
                transitionEnter = {false}
                transitionLeave = {false}>
                {product && product.code &&
                    <ProductDetailItem
                        {...product}
                        isLoginUser={isLoginUser}
                        cartErrorMsg={cartErrorMsg}
                        isFetching={isFetching}
                        onAddToCart={onAddToCart}
                        onResetCartErrorMsg={onResetCartErrorMsg}
                        onBackToList={onBackToList}
                    />
                }
            </ReactCSSTransitionGroup>
        );
    }
}

ProductDetailMain.propTypes = {
    product: PropTypes.object.isRequired,
    isLoginUser: PropTypes.bool.isRequired,
    cartErrorMsg: PropTypes.string,
    isFetching: PropTypes.bool.isRequired,
    onAddToCart: PropTypes.func.isRequired,
    onResetCartErrorMsg: PropTypes.func.isRequired,
    onBackToList: PropTypes.func.isRequired
};
