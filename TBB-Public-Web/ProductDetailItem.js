import Carousel from 'nuka-carousel';
import { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import Constants = from './constants';
import Decorators from '../../components/common/CarouselDecorator';
import FreeSampleSeal from '../../components/common/FreeSampleSeal';
import McAfeeTrustSeal from '../../components/common/McAfeeTrustSeal';
import ProductDescription from '../../components/common/ProductDescription';
import SocialShare from '../../components/common/SocialShare';
import asset from '../../redux/constants/assetEndpoints';
import AddToCartInDetail from '../cart/AddToCartInDetail';

export default class ProductDetailItem extends Component {
    componentDidMount() {
        /*eslint-disable */
        if (typeof yotpo !== 'undefined') {
            yotpo.initWidgets();
        }
        /*eslint-enable */
    }

    render() {
        const { id, code, name, mainTitle, subTitle, displaySize, description, images,
            ingredientList, ingredientsNote, howToUse, faq, productVarieties,
            isSample, isLoginUser, cartErrorMsg, isFetching, onAddToCart, onResetCartErrorMsg, onBackToList } = this.props;
        const imgRootPath = `${asset.PRODUCT_LIST_IMAGE_URL}${code}/`;
        const firstImgPath = imgRootPath + images[0];

        return (
            <div className="ui container">
                <h1 className="ui center aligned header">
                    <div className="content">
                        {mainTitle}
                        {displaySize && <a className="ui circular label tbb">{displaySize}</a>}
                        <br />
                        {subTitle}
                        <div className="yotpo bottomLine"
                             data-product-id={code}
                             data-product-models="Products model information"
                             data-name={name}
                             data-image-url={imgRootPath + images[0]}
                             data-description={description}>
                        </div>
                    </div>
                </h1>
                <div className="ui stackable two column relaxed grid">
                    <div className="column">
                        {isSample && <FreeSampleSeal pageType={Constants.Seal.ProductDetail} />}
                        <Carousel decorators={Decorators} wrapAround={true}>
                            {images.map(img => {
                                return (
                                    <img key={`product_image_${id}_${img}`} src={imgRootPath + img} />
                                );
                            })}
                        </Carousel>
                    </div>
                    <div className="column">
                        <ProductDescription
                            description={description}
                            ingredientList={ingredientList}
                            ingredientsNote={ingredientsNote}
                            howToUse={howToUse}
                            faq={faq}
                        />
                        <AddToCartInDetail
                            productId={id}
                            productCode={code}
                            productVarieties={productVarieties}
                            isSample={isSample}
                            isLoginUser={isLoginUser}
                            cartErrorMsg={cartErrorMsg}
                            isFetching={isFetching}
                            onAddToCart={onAddToCart}
                            onResetCartErrorMsg={onResetCartErrorMsg} />
                        <div className="ui left floated basic segment">
                            <a onClick={onBackToList}>
                                <i className="arrow circle outline left icon" />See other products
                            </a>
                        </div>
                        <div className="ui right floated basic segment">
                            <McAfeeTrustSeal />
                        </div>
                    </div>
                </div>
                <div className="yotpo yotpo-main-widget"
                     data-product-id={code}
                     data-name={name}
                     data-image-url={firstImgPath}
                     data-description={description}>
                </div>
                <SocialShare path={'/product/' + code} title={name} mediaUrl={firstImgPath} />
            </div>
        );
    }
}

ProductDetailItem.propTypes = {
    id: PropTypes.number.isRequired,
    code: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    mainTitle: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    displaySize: PropTypes.string,
    description: PropTypes.string.isRequired,
    ingredientList: PropTypes.array.isRequired,
    ingredientsNote: PropTypes.array,
    howToUse: PropTypes.array,
    faq: PropTypes.array,
    images: PropTypes.array.isRequired,
    productVarieties: PropTypes.array.isRequired,
    actualProduct: PropTypes.object,
    sampleProductCode: PropTypes.string,
    isSample: PropTypes.bool.isRequired,
    isLoginUser: PropTypes.bool.isRequired,
    cartErrorMsg: PropTypes.string,
    isFetching: PropTypes.bool.isRequired,
    onAddToCart: PropTypes.func.isRequired,
    onResetCartErrorMsg: PropTypes.func.isRequired,
    onBackToList: PropTypes.func.isRequired
};
